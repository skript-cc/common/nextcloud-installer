<?php

declare(strict_types=1);

namespace Skript\Composer\Nextcloud;

use Composer\Package\PackageInterface;
use PHPUnit\Framework\TestCase;

class InstallerTest extends TestCase
{
    public function testFindInstallPathByType(): void
    {
        $installerPaths = [
            'public' => ['type:nextcloud-server']
        ];
        $foundPath = Installer::findInstallPath(
            'nextcloud-server',
            'nextcloud/server',
            $installerPaths
        );
        $this->assertEquals('public', $foundPath);
    }
    
    public function testFindInstallPathByPackageName(): void
    {
        $installerPaths = [
            'public' => ['nextcloud/server']
        ];
        $foundPath = Installer::findInstallPath(
            '',
            'nextcloud/server',
            $installerPaths
        );
        $this->assertEquals('public', $foundPath);
    }
    
    public function testFindInstallPathReplacesName(): void
    {
        $installerPaths = [
            'apps-extra/{$name}' => ['type:nextcloud-app']
        ];
        $foundPath = Installer::findInstallPath(
            'nextcloud-app',
            'nextcloud/awesome-app',
            $installerPaths
        );
        $this->assertEquals('apps-extra/awesome-app', $foundPath);
    }
    
    public function testInstallerSupportsNextcloudServer(): void
    {
        [$installer] = ComposerFactory::createInstaller();
        $this->assertTrue($installer->supports('nextcloud-server'));
    }
    
    public function testInstallerSupportsNextcloudApps(): void
    {
        [$installer] = ComposerFactory::createInstaller();
        $this->assertTrue($installer->supports('nextcloud-app'));
    }
    
    public function testGetInstallPathReturnsCustomPathForServerPackage(): void
    {
        [$installer] = ComposerFactory::createInstaller([
            'extra' => [
                'installer-paths' => [
                    'public' => ['type:nextcloud-server'],
                    'public/apps-extra/{$name}' => ['type:nextcloud-app']
                ]
            ]
        ]);
        
        $package = $this->createStub(PackageInterface::class);
        $package->method('getName')->willReturn('nextcloud/server');
        $package->method('getType')->willReturn('nextcloud-server');
        
        $this->assertEquals(
            'public',
            $installer->getInstallPath($package)
        );
    }
    
    public function testGetInstallPathReturnsDefaultPathForServerPackage(): void
    {
        [$installer, $composer] = ComposerFactory::createInstaller([
            'extra' => [
                'installer-paths' => [
                    'public/apps-extra/{$name}' => ['type:nextcloud-app']
                ]
            ]
        ]);
        
        $package = $this->createStub(PackageInterface::class);
        $package->method('getName')->willReturn('nextcloud/server');
        $package->method('getType')->willReturn('nextcloud-server');
        
        $this->assertEquals(
            rtrim($composer->getConfig()->get('vendor-dir'), DIRECTORY_SEPARATOR),
            rtrim($installer->getInstallPath($package), DIRECTORY_SEPARATOR)
        );
    }
    
    public function testGetInstallPathReturnsDefaultPathForAppPackage(): void
    {
        [$installer, $composer] = ComposerFactory::createInstaller([
            'extra' => [
                'installer-paths' => [
                    'public' => ['type:nextcloud-server']
                ]
            ]
        ]);
        
        $package = $this->createStub(PackageInterface::class);
        $package->method('getName')->willReturn('nextcloud/awesome-app');
        $package->method('getType')->willReturn('nextcloud-app');
        
        $this->assertEquals(
            rtrim($composer->getConfig()->get('vendor-dir'), DIRECTORY_SEPARATOR),
            rtrim($installer->getInstallPath($package), DIRECTORY_SEPARATOR)
        );
    }
    
    public function testGetInstallPathReturnsCustomPathForAppPackage(): void
    {
        [$installer] = ComposerFactory::createInstaller([
            'extra' => [
                'installer-paths' => [
                    'public' => ['type:nextcloud-server'],
                    'public/apps-extra/{$name}' => ['type:nextcloud-app']
                ]
            ]
        ]);
        
        $package = $this->createStub(PackageInterface::class);
        $package->method('getName')->willReturn('nextcloud/awesome-app');
        $package->method('getType')->willReturn('nextcloud-app');
        
        $this->assertEquals(
            'public/apps-extra/awesome-app',
            $installer->getInstallPath($package)
        );
    }
}