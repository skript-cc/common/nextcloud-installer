<?php

declare(strict_types=1);

namespace Skript\Composer\Nextcloud;

use Composer\Package\PackageInterface;
use Composer\Script\ScriptEvents;
use PHPUnit\Framework\TestCase;

use function Skript\Utils\FileSystem\{mkdir, rm};
use function Skript\Utils\Path\join;

class InstallerPluginTest extends TestCase
{
    protected $tmpDir;
    protected $cwd;
    protected $testInstallTests = [
        'testPostInstallEventMovesServerInstallBack',
        'testPostInstallEventCleansupServerInstall',
        'testPostInstallEventSymlinksConfig',
        'testInstallDoesNotRemoveFilesToKeep'
    ];
    
    protected function setup(): void
    {
        if (in_array($this->getName(false), $this->testInstallTests)) {
            $this->setupTestInstall();
        }
    }
    
    protected function setupTestInstall(): void
    {
        $this->tmpDir = $tmpDir = join(__DIR__, 'tmp-'.uniqid());
        $tmpInstallDir = join($tmpDir, 'nextcloud', '_');
        $customConfigDir = join($tmpDir, 'config');
        $configDir = join($tmpInstallDir, 'config');
        
        mkdir($tmpInstallDir);
        mkdir($customConfigDir);
        mkdir($configDir);
        
        touch(join($customConfigDir, 'server.php'));
        touch(join($tmpInstallDir, 'index.php'));
        touch(join($configDir, 'CAN_INSTALL'));
        touch(join($configDir, 'config.sample.php'));
        
        $this->cwd = getcwd();
        chdir($tmpDir);
    }
    
    protected function teardown(): void
    {
        if (in_array($this->getName(false), $this->testInstallTests)) {
            $this->teardownTestInstall();
        }
    }
    
    protected function teardownTestInstall(): void
    {
        rm($this->tmpDir);
        chdir($this->cwd);
    }
    
    public function testActivateAddsInstaller(): void
    {
        $composer = ComposerFactory::createComposer();
        $plugin = ComposerFactory::createAndActivatePlugin($composer);
        
        $this->assertInstanceOf(
            Installer::class,
            $composer->getInstallationManager()->getInstaller('nextcloud-server')
        );
    }
    
    public function testActivateSetsTempServerInstallPath(): void
    {
        $composer = ComposerFactory::createComposer([
            'extra' => [
                'installer-paths' => [
                    'public' => ['type:nextcloud-server']
                ]
            ]
        ]);
        $plugin = ComposerFactory::createAndActivatePlugin($composer);
        
        $package = $this->createStub(PackageInterface::class);
        $package->method('getName')->willReturn('nextcloud/server');
        $package->method('getType')->willReturn('nextcloud-server');
        
        $installer = (
            $composer
                ->getInstallationManager()
                ->getInstaller('nextcloud-server')
        );
        
        $this->assertEquals(
            implode(DIRECTORY_SEPARATOR, ['public','_']),
            $installer->getInstallPath($package)
        );
    }
    
    public function testActivateSetsDefaultServerInstallPath(): void
    {
        $composer = ComposerFactory::createComposer();
        $plugin = ComposerFactory::createAndActivatePlugin($composer);
        
        $package = $this->createStub(PackageInterface::class);
        $package->method('getName')->willReturn('nextcloud/server');
        $package->method('getType')->willReturn('nextcloud-server');
        
        $installer = (
            $composer
                ->getInstallationManager()
                ->getInstaller('nextcloud-server')
        );
        
        $this->assertEquals(
            implode(DIRECTORY_SEPARATOR, ['nextcloud','_']),
            $installer->getInstallPath($package)
        );
    }
    
    /**
     * @dataProvider eventProvider
     */
    public function testPostInstallEventMovesServerInstallBack(string $event): void
    {
        $composer = ComposerFactory::createComposer();
        $plugin = ComposerFactory::createAndActivatePlugin($composer);
        $dispatcher = ComposerFactory::createEventDispatcher($composer);
        $dispatcher->addSubscriber($plugin);
        
        $dispatcher->dispatchScript($event);
        
        $this->assertFileExists(join($this->tmpDir, 'nextcloud', 'index.php'));
    }
    
    /**
     * @dataProvider eventProvider
     */
    public function testPostInstallEventCleansupServerInstall(string $event): void
    {
        $composer = ComposerFactory::createComposer();
        $plugin = ComposerFactory::createAndActivatePlugin($composer);
        $dispatcher = ComposerFactory::createEventDispatcher($composer);
        $dispatcher->addSubscriber($plugin);
        
        $dispatcher->dispatchScript($event);
        
        $this->assertFileNotExists(join($this->tmpDir, 'nextcloud', 'config', 'CAN_INSTALL'));
        $this->assertFileNotExists(join($this->tmpDir, 'nextcloud', 'config', 'config.sample.php'));
    }
    
    /**
     * @dataProvider eventProvider
     */
    public function testPostInstallEventSymlinksConfig(string $event): void
    {
        $composer = ComposerFactory::createComposer();
        $plugin = ComposerFactory::createAndActivatePlugin($composer);
        $dispatcher = ComposerFactory::createEventDispatcher($composer);
        $dispatcher->addSubscriber($plugin);
        
        $dispatcher->dispatchScript($event);
        
        $customConfigSymlink = join($this->tmpDir, 'nextcloud', 'config', 'custom.config.php');
        
        $this->assertTrue(is_link($customConfigSymlink));
        $this->assertEquals(
            '../../config/server.php',
            readlink($customConfigSymlink)
        );
    }
    
    public function testInstallDoesNotRemoveFilesToKeep(): void
    {
        $htaccessFile = join($this->tmpDir, 'nextcloud', '.htaccess');
        $configFile = join($this->tmpDir, 'nextcloud', 'config', 'config.php');
        
        file_put_contents($htaccessFile, 'MODIFIED_CONTENT');
        mkdir(dirname($configFile));
        file_put_contents($configFile, 'MODIFIED_CONTENT');
        
        $composer = ComposerFactory::createComposer([
            'extra' => [
                'nextcloud-installer' => [
                    'keep' => [
                        '.htaccess' => true,
                        'config/config.php' => true,
                        'config/config.sample.php' => false,
                        'config/CAN_INSTALL' => false
                    ]
                ]
            ]
        ]);
        $plugin = ComposerFactory::createAndActivatePlugin($composer);
        $dispatcher = ComposerFactory::createEventDispatcher($composer);
        $dispatcher->addSubscriber($plugin);
        
        $dispatcher->dispatchScript(ScriptEvents::POST_INSTALL_CMD);
        
        $this->assertFileExists($htaccessFile);
        $this->assertFileExists($configFile);
        
        $this->assertEquals('MODIFIED_CONTENT', file_get_contents($htaccessFile));
        $this->assertEquals('MODIFIED_CONTENT', file_get_contents($configFile));
    }
    
    public function eventProvider(): array
    {
        return [
            [ScriptEvents::POST_INSTALL_CMD],
            [ScriptEvents::POST_UPDATE_CMD]
        ];
    }
}