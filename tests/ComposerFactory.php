<?php

declare(strict_types=1);

namespace Skript\Composer\Nextcloud;

use Composer\EventDispatcher\EventDispatcher;
use Composer\Composer;
use Composer\IO\NullIO;
use Composer\Factory;

class ComposerFactory
{
    public static function createComposer(array $localConfig = []): Composer
    {
        $io = new NullIO();
        $composer = (new Factory())->createComposer($io, $localConfig);
        
        return $composer;
    }
    
    public static function createAndActivatePlugin(Composer $composer): InstallerPlugin
    {
        $plugin = new InstallerPlugin();
        $plugin->activate($composer, new NullIO());
        
        return $plugin;
    }
    
    public static function createInstaller(array $localConfig = []): array
    {
        $io = new NullIO();
        $composer = (new Factory())->createComposer($io, $localConfig);
        
        return [
            new Installer($io, $composer),
            $composer
        ];
    }
    
    public static function createEventDispatcher(Composer $composer): EventDispatcher
    {
        return new EventDispatcher($composer, new NullIO());
    }
}