# Nextcloud Installer

Composer installer for Nextcloud server and apps packages to install them at a
location outside the vendor directory. Made to be used in conjuction with the 
[Nextcloud Packages Composer Repository](https://skript-cc.gitlab.io/common/nextcloud-packages/).

## Installation

```sh
composer require skript.cc/nextcloud-installer
```

## Usage

In your composer.json file you need to add the Nextcloud Packages Repo as custom 
repository.

```json
{
  "repositories": [{
    "type": "composer",
    "url": "https://skript-cc.gitlab.io/common/nextcloud-packages"
  }]
}
```

Next, require the `nextcloud/server` package and run composer install.

```sh
composer require nextcloud/server
composer install
```

### Customizing the installation directory

By default Nextcloud will be installed in the directory `nextcloud/` inside your
project root. It is possible to customize this location by adding a custom
installer path. The following example will make Nextcloud install in the
directory `public/`.

```json
{
    "extra": {
        "installer-paths": {
            "public": ["type:nextcloud-server"]
        }
    }
}
```

It is also possible to modify the installation path of Nextcloud apps.

```json
{
    "extra": {
        "installer-paths": {
            "public": ["type:nextcloud-server"],
            "public/apps-extra/{$name}": ["type:nextcloud-app"]
        }
    }
}
```

### Keeping files inside Nextclouds installation directory

By default all files and directories in the Nextcloud installation directory
will be overwritten by the files that are provided by the nextcloud/server 
package. For example, when you add a file  `nextcloud/.htaccess` and then 
run `composer install` (or update), your file gets overwritten by the `.htaccess`
file from the nextcloud package.

The installer comes preconfigured with two exceptions to this rule: 
`config/config.php` and `data/` will always be kept.

With a little bit of configuration in the composer.json file it is possible to
add your own rules for keeping (and removing) files. The following example makes
the .htaccess file that is added or modified by you persistent between updates
and installs.

```json
{
    "extra": {
        "nextcloud-installer": {
            "keep": {
                ".htaccess": true
            }
        }
    }
}
```

Files can also be removed from the installation directory by changing the value
of the filepath to `false`. When you do this in the example above there won't be
a .htaccess file present at all after the installation. By default 
`config/CAN_INSTALL` and `config/config.sample.php` are removed.

### Moving the configuration file out of the installation directory

With the keep configuration explained in the previous section it is possible
to make configuration files persist between installations while being placed
inside Nextclouds default configuration directory. When you don't like this, the
installer also supports another option to persist configuration settings.

When you place a file `config/server.php` (relative to the project root) the 
installer will symlink this file to Nextclouds configuration directory. Any 
configuration setting defined in this file will override the setting from
Nextclouds default `config/config.php`.
