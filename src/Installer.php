<?php

namespace Skript\Composer\Nextcloud;

use Composer\Config;
use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;

class Installer extends LibraryInstaller
{
    const TYPE_SERVER = 'nextcloud-server';
    const TYPE_APP = 'nextcloud-app';
    
    public static function findInstallPath(
        string $packageType,
        string $packageName,
        array $installerPaths
    ): ?string {
        $paths = [];
        $packageTypes = [];
        
        if (!empty($packageType)) {
            $packageTypes[] = 'type:'.$packageType;
        }
        
        if (!empty($packageName)) {
            [$vendor, $name] = explode('/', $packageName);
            $packageTypes[] = $packageName;
        }
        
        foreach ($installerPaths as $path => $pathTypes) {
            $intersection = array_intersect($packageTypes, $pathTypes);
            if (count($intersection) > 0) {
                $paths[] = isset($name) 
                    ? strtr($path, ['{$name}' => $name])
                    : $path;
            }
        }
        
        return count($paths) > 0 ? end($paths) : null;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package): string
    {
        $extra = $this->composer->getPackage()->getExtra();
        
        $path = self::findInstallPath(
            $package->getType(),
            $package->getName(),
            $extra['installer-paths'] ?? []
        );
        
        return $path ?? parent::getInstallPath($package);
    }
    
    /**
     * {@inheritDoc}
     */
    public function supports($packageType): bool
    {
        return in_array($packageType, [self::TYPE_APP, self::TYPE_SERVER]);
    }
}