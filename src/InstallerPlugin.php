<?php

namespace Skript\Composer\Nextcloud;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Composer\Plugin\PluginInterface;
use Skript\Utils\{FileSystem, Path};

require_once __DIR__.'/Installer.php';

class InstallerPlugin implements EventSubscriberInterface, PluginInterface
{
    protected $origInstallDir;
    protected $tmpInstallDir;
    protected $autoloadPath;
    protected $filesToKeep = [];
    protected $filesToClean = [];
    
    public function activate(Composer $composer, IOInterface $io): void
    {
        $im = $composer->getInstallationManager();
        $im->addInstaller(new Installer($io, $composer));
        
        $this->autoloadPath = (
            $composer->getConfig()->get('vendor-dir')
            .DIRECTORY_SEPARATOR
            .'autoload.php'
        );
        
        $package = $composer->getPackage();
        $defaultKeepConfig = [
            // keep
            implode(DIRECTORY_SEPARATOR, ['config', 'config.php']) => true,
            implode(DIRECTORY_SEPARATOR, ['data']) => true,
            // clean
            implode(DIRECTORY_SEPARATOR, ['config', 'config.sample.php']) => false,
            implode(DIRECTORY_SEPARATOR, ['config', 'CAN_INSTALL']) => false
        ];
        $keepConfig = array_merge(
            $defaultKeepConfig,
            $package->getExtra()['nextcloud-installer']['keep'] ?? []
        );
        
        $this->setServerInstallDir($package);
        $this->setFilesToKeep($keepConfig);
        $this->setFilesToClean($keepConfig);
    }
    
    public static function getSubscribedEvents(): array
    {
        return [
            'post-autoload-dump' => 'onPostAutoloadDump',
            'post-install-cmd' => 'modifyServerInstall',
            'post-update-cmd' => 'modifyServerInstall'
        ];
    }

    public function onPostAutoloadDump(): void
    {
        // Why this? On the first install this plugin is loaded without an
        // autoloader. That means that the dependencies required by this 
        // installer package are not installed and loaded yet. That's why they
        // need to be loaded manually here.
        
        $loader = require $this->autoloadPath;
        if (!class_exists('Skript\Utils\FileSystem')) {
            $loader->loadClass('Skript\Utils\FileSystem');
        }
        if (!class_exists('Skript\Utils\Path')) {
            $loader->loadClass('Skript\Utils\Path');
        }
    }

    public function modifyServerInstall(): void
    {
        $this->moveTmpServerInstallBack();
        $this->cleanServerInstall();
        $this->symlinkConfigFile();
    }
    
    /**
     * {@inheritDoc}
     */
    public function uninstall(Composer $composer, IOInterface $io)
    {
        // intentionally left blank
    }
    
    /**
     * {@inheritDoc}
     */
    public function deactivate(Composer $composer, IOInterface $io)
    {
        // intentionally left blank
    }
    
    protected function setServerInstallDir(PackageInterface $package): void
    {
        $extra = $package->getExtra();
        $this->origInstallDir =  Installer::findInstallPath(
            'nextcloud-server',
            'nextcloud/server',
            $extra['installer-paths'] ?? []
        );
        
        if (!$this->origInstallDir) {
            $this->origInstallDir = 'nextcloud';
            $this->tmpInstallDir = implode(DIRECTORY_SEPARATOR, ['nextcloud','_']);
        } else {
            $this->tmpInstallDir = (
                rtrim($this->origInstallDir, DIRECTORY_SEPARATOR)
                . DIRECTORY_SEPARATOR
                . '_'
            );
        }
        
        // place the server install inside a temp directory
        $extra['installer-paths'] = array_merge(
            $extra['installer-paths'] ?? [],
            [$this->tmpInstallDir => ['type:nextcloud-server']]
        );
        $package->setExtra($extra);
    }
    
    protected function setFilesToKeep(array $keepConfig): void
    {
        $this->filesToKeep = array_keys(
            array_filter(
                $keepConfig,
                function (bool $keep) {
                    return $keep === true;
                }
            )
        );
    }
        
    protected function setFilesToClean(array $keepConfig): void
    {
        $this->filesToClean = array_keys(
            array_filter(
                $keepConfig,
                function (bool $keep) {
                    return $keep === false;
                }
            )
        );
    }
    
    protected function moveTmpServerInstallBack(): void
    {
        // move files to keep to the newly installed nextcloud directory
        foreach ($this->filesToKeep as $file) {
            $tmpFile = Path::join($this->tmpInstallDir, $file);
            $origFile = Path::join($this->origInstallDir, $file);
            if (!file_exists($origFile)) {
                continue;
            }
            FileSystem::rm($tmpFile);
            rename($origFile, $tmpFile);
        }
        
        // then move the new install to the original location
        $paths = array_diff(scandir($this->tmpInstallDir), ['..', '.']);
        foreach ($paths as $path) {
            FileSystem::rm(Path::join($this->origInstallDir, $path));
            rename(
                Path::join($this->tmpInstallDir, $path),
                Path::join($this->origInstallDir, $path)
            );
        }
        rmdir($this->tmpInstallDir);
    }
    
    protected function cleanServerInstall(): void
    {
        foreach ($this->filesToClean as $file) {
            try {
                FileSystem::rm(Path::join($this->origInstallDir, $file));
            } catch (\ErrorException $e) {
                // fail silently when rm fails
            }
        }
    }
    
    protected function symlinkConfigFile(): void
    {
        $customConfigFile = Path::join('config', 'server.php');
        if (!file_exists($customConfigFile)) {
            return;
        }
        FileSystem::symlink(
            $customConfigFile,
            Path::join($this->origInstallDir, 'config', 'custom.config.php')
        );
    }
}