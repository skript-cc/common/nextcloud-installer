# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2021-01-06
### Added
- Added composer 2.0 support
- Added files to keep configuration, to make files and directories (such as config/config.php and data/) persistent between installs (#1)

## [0.1.0] - 2019-10-29
### Added
- Installation and usage instructions
- Post installation step to symlink custom configuration file
- Post installation step to cleanup the nextcloud server installation
- Some magic to be able to install nextcloud apps inside the nextcloud installation directory
- Custom installer paths for nextcloud-server and nextcloud-app package types

[Unreleased]: https://gitlab.com/skript-cc/common/nextcloud-installer/compare/0.2.0...master
[0.2.0]: https://gitlab.com/skript-cc/common/nextcloud-installer/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/skript-cc/common/nextcloud-installer/-/tags/0.1.0